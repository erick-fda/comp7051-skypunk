==========================================================================================
    COMP7051 Term Project
    
    Copyright 2017 Erick Fernandez de Arteaga
        https://www.linkedin.com/in/erick-fda
        https://bitbucket.org/erick-fda
        https://github.com/erick-fda
    
==========================================================================================

------------------------------------------------------------------------------------------
    TABLE OF CONTENTS
    
    ## Overview
    
------------------------------------------------------------------------------------------

==========================================================================================
    ## Overview
==========================================================================================
    This project is my group's term project from course COMP7051 at the British Columbia 
    Institute of Technology. The project was created during the fall term (September - 
    December) of 2016.
    
    The group members were Erick Fernandez de Arteaga, Denis Turitsa, Michael Nation, and 
    Gui Togniolo.
    