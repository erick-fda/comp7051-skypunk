=============================================================================================
    COMP 7051 Term Project
    
    Denis Turitsa
    Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
    Guilherme Togniolo dos Santos
    Michael Nation
    
=============================================================================================

---------------------------------------------------------------------------------------------
    TABLE OF CONTENTS
    
    ## Controls
    ## Platforms
    ## Documentation
    
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
    ## Controls
---------------------------------------------------------------------------------------------
    KEYBOARD/MOUSE
    - Move mouse to control flight direction.
    - Left click to breather fire.
    - W/A to speed up and slow down.
    - A/D to roll left and right.
    - ESC to pause the game.
    
    CONTROLLER
    - Left stick movement to control flight direction.
    - Right stick movement to speed up, slow down, and roll left and right.
    - Right trigger to breathe fire.
    - START to pause (Player 1 only if playing multiplayer)
    
    The objective of the game is to destroy as many non-dragon mobs
    as possible! The player team score is increased with each enemy mob 
    destroyed and decreased with each player death.
    
---------------------------------------------------------------------------------------------
    ## Platforms
---------------------------------------------------------------------------------------------
    The game has been ported to Windows, OSX, and Linux platforms.
    
---------------------------------------------------------------------------------------------
    ## Documentation
---------------------------------------------------------------------------------------------
    For detailed code documentation, open 
    ./Doxygen/html/index.html 
    in a modern web browser.
    