using UnityEngine;
using System;
using UnityEngine.SceneManagement;

/**Controls player movement*/
public class PlayerController : MonoBehaviour {

	/**The player's number, used for input controller*/
	public int playerNumber = 1;

    /**Maximum speed the player can go*/
    public float maxSpeed = 40;
    
    /**minimum speed the player can move*/
    public float minSpeed = 10;

    /**how quickly the player accelerates from the current speed to the desired speed*/
    public float acceleration = 20;

    /**The player's standard speed, without speeding up or slowing down*/
    public float defaultSpeed;

    /** the camera associated with the player*/
    public Camera playerCamera;

    /**the maximum yaw turn rate (left/right turning)*/
    public float yawRate;

    /**the maximum pitch rate (up/down turning)*/ 
    public float pitchRate;

    /** the maximum roll rate of the player*/
    public float rollRate;

    /**the player model*/
    public GameObject ship;

    /**how much the model moves relative to the player parent object (yaw direction)*/
    public float yawTilt;

    /**how much the model moves relative to the player parent object (pitch direction)*/
    public float pitchTilt;

    /**how much the model moves relative to the player parent object (roll direction)*/
    public float rollTilt;

    /** the position of the mouse on the screen*/
    private Vector2 mousePosition;

    /**the player's rigidbody*/
    private Rigidbody rb;

    /**the players current speed */
    public float currentSpeed;

    /**how quickly the ship will roll upright if left alone*/
    public float rightingSpeed = .2f;

    /**the Shoot object of the player. responsible for weapon firing*/
    private Shoot shoot;

    /**The default camera position, before any zooming is applied*/
    private Vector3 defaultCameraPosition;

    /**the camera zoom change. Used for camera zooming when accelerating*/
    private float cameraScale = 1;

	public float numberOfControllers = 0;

    /* Axis name suffix for the current runtime platform. 
        Should be _WIN, _OSX, or _LNX. */
    private string platformSuffix = "_WIN";

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody>();
        shoot = GetComponent<Shoot>();
        defaultCameraPosition = playerCamera.transform.localPosition;

        /* Get the current platform. */
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                platformSuffix = "_WIN";
                break;
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.OSXPlayer:
                platformSuffix = "_OSX";
                break;
            case RuntimePlatform.LinuxPlayer:
                platformSuffix = "_LNX";
                break;
            default:
                throw new IRefs.UnsupportedPlatformException("The current application " +
                    "platform is not supported. Only Windows, OSX, and " +
                    "Linux platforms are supported.");
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        /* Get the number of controllers connected. */
        numberOfControllers = GetControllerCount();

        //restarts the scene on esc key. TODO: replace this with an actual menu
        //if (Input.GetKeyDown("escape"))
        //    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		if (numberOfControllers < 1) {
			getMousePosition ();
        }
        rotateShip();
        turnPlayer();
        rollPlayer();
        movePlayer();
        rollUpright();

		if (Input.GetAxis("P" + playerNumber + "TriggerFire" + platformSuffix) > 0.1f || Input.GetButton("P" + playerNumber + "MouseFire"))
            shoot.fire();
    }

    void getMousePosition() {
        mousePosition = Input.mousePosition;
        mousePosition.x = (mousePosition.x - (Screen.width / 2)) / (Screen.width / 2) * 2;
        mousePosition.y = (mousePosition.y - (Screen.height / 2)) / (Screen.height / 2) * 2;

        if (mousePosition.x > 1)
            mousePosition.x = 1;
        if (mousePosition.x < -1)
            mousePosition.x = -1;

        if (mousePosition.y > 1)
            mousePosition.y = 1;
        if (mousePosition.y < -1)
            mousePosition.y = -1;
    }

    void turnPlayer() {
        /* If no controllers are connected, turn with mouse. */
		if (numberOfControllers < 1)
        {
			Vector3 turnDirection = new Vector3 (-mousePosition.y, mousePosition.x, 0);
			transform.Rotate (turnDirection * ((yawRate * Mathf.Abs (mousePosition.x)) + (pitchRate * Mathf.Abs (mousePosition.y))) * Time.deltaTime);
		}
        /* If controllers are connected, turn with left analog stick. 
            X and Y axes are cross-platform, so no platform check is required. */
        else
        {
			transform.RotateAround (transform.position, transform.up, Time.deltaTime * Input.GetAxis("P" + playerNumber + "Horizontal") * pitchRate);
			transform.RotateAround (transform.position, transform.right, Time.deltaTime * Input.GetAxis("P" + playerNumber + "Vertical") * pitchRate);


//			Vector3 turnDirection = new Vector3 (Input.GetAxis("P" + playerNumber + "Vertical"), Input.GetAxis("P" + playerNumber + "Horizontal"), 0);
//			transform.rotation = Quaternion.LookRotation (turnDirection * Time.deltaTime);
//			transform.Rotate (turnDirection * Time.deltaTime);
		}
			
    }

    /**Sets the player's forward movement, acceleration, and associated camera zooming*/
    void movePlayer()
    {
        
        if (Input.GetAxis("P" + playerNumber + "Boost" + platformSuffix) > 0 && currentSpeed < maxSpeed)
        {
            currentSpeed += acceleration * Time.deltaTime;
		}

        else if (Input.GetAxis("P" + playerNumber + "Boost" + platformSuffix) < 0 && currentSpeed > minSpeed)
        {
            currentSpeed -= acceleration * Time.deltaTime;
        }

        else
        {
            currentSpeed += Math.Sign(defaultSpeed - currentSpeed) *  acceleration * .3f * Time.deltaTime;
        }

        //moves the camera to give visual feedback when moving at increased or decreased speed
        if(currentSpeed < defaultSpeed && currentSpeed > minSpeed + 1) {
            cameraScale = currentSpeed / defaultSpeed + ((1 - currentSpeed / defaultSpeed) * .5f);
        }
        else if(currentSpeed > defaultSpeed && currentSpeed < maxSpeed - 1) {
            cameraScale = currentSpeed / defaultSpeed;
        }
        playerCamera.transform.localPosition = Vector3.Scale(defaultCameraPosition, new Vector3(1, 1, 1) * cameraScale);


        //move the ship forward using rigidbody - speed is in m/s  (standard for unity)
        if (Vector3.Project(rb.velocity, transform.forward).magnitude < currentSpeed)
            rb.AddForce(transform.forward * 300, ForceMode.Acceleration);
    }

    void rollPlayer()
    {
        transform.Rotate(transform.InverseTransformDirection(-Input.GetAxis("P" + playerNumber + "Roll" + platformSuffix) * transform.forward) * rollRate * Time.deltaTime);
    }

    void rotateShip()
    {
        if (numberOfControllers < 1)
        {
            ship.transform.localEulerAngles = new Vector3(-mousePosition.y * pitchTilt, mousePosition.x * yawTilt, -mousePosition.x * rollTilt);
        }
        else
        {
            ship.transform.localEulerAngles = Vector3.zero;
        }
    }

    /**Rolls the player object so that it is right side up.*/
    private void rollUpright()
    {
        float step = rightingSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.up, Vector3.up, step, 0.0F);
        transform.rotation = Quaternion.LookRotation(transform.forward, newDir);
    }

    /**
        Gets the number of controllers that are currently connected.
    */
    private int GetControllerCount()
    {
        int controllerCount = 0;

        foreach (string i in Input.GetJoystickNames())
        {
            if (i != null && i != string.Empty)
            {
                controllerCount++;
            }
        }

        return controllerCount;
    }
}
