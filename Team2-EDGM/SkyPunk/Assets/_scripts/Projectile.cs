﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public float velocity = 100;

    public float lifeTime = 1;

    public GameObject owner;

    public float damage = 10f;

    // Use this for initialization
    void Start() {
        Object.Destroy(gameObject, lifeTime);
    }

    // Update is called once per frame
    void Update() {

    }

    void OnCollisionEnter(Collision col) {
        Destroy(this.gameObject);
    }
}
