﻿/*===========================================================================================
    SaveData                                                                            *//**
	
	A Serializable Object that stores a player's save data.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
*//*=======================================================================================*/

/*===========================================================================================
	Dependencies
===========================================================================================*/
using System;
using System.Collections.Generic;

/*===========================================================================================
	SaveData
===========================================================================================*/
/**
	A Serializable Object that stores a player's save data.
*/
[Serializable]
public class SaveData : Object
{
    /*=======================================================================================
		Fields
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		Public
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Private
	---------------------------------------------------------------------------------------*/
    /**
        The list of player high scores.

        Initialize all entries to the minimum permitted score value.
    */
    private List<int> leaderboard = new List<int>
    {
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore,
        Refs.Scoring.MinimumTeamScore
    };

    /*=======================================================================================
		Properties
	=======================================================================================*/
    /**
        Public access property for SaveData.leaderboard.

        @see SaveData.leaderboard
    */
    public List<int> Leaderboard
    {
        get { return leaderboard; }

        set { leaderboard = value; }
    }

    /*=======================================================================================
		Methods
	=======================================================================================*/
    /**
        Adds the parameter score to the leaderboard if it is greater than the lowest score 
        currently on the leaderboard. Replaces the lowest score on the leaderboard.

        Returns true if the score was added to the leaderboard and false if it was too low.

        @returns True if the score was added to the leaderboard and false if it was too low.

        @param score - The score to try adding to the leaderboard.
    */
    public bool TryHighScore(int score)
    {
        /* Sort the leaderboard scores. */
        Leaderboard.Sort();

        /* Add the score to the leaderboard if it is greater than the smallest score. 
            Then return true. */
        if (score > Leaderboard[0])
        {
            /* Update the leaderboard. */
            Leaderboard[0] = score;
            return true;
        }
        /* If the score was too low to make it onto the leaderboard, return false. */
        else
        {
            return false;
        }
    }
}
