﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LabelController : MonoBehaviour {

    /**main camera to orient the label towards*/
    private Camera mainCamera;

    /** textbox to display name*/
    public string nameString = "Unspecified";

    public Text labelText;
   
    /**is the object friendly (used to determine color of label)*/
    public bool isFriendly = false;

    /**the gameobject of the guitext to be created*/
    private GameObject textGameobject;

    private Color labelColor;

    void Start () {
        mainCamera = Camera.main;
        if(labelText == null)
            labelText = GetComponentInChildren<Text>();

        labelText.text = nameString;

        if (isFriendly)
            labelText.color = new Color(0, 1, 0, .7f);
        else
            labelText.color = new Color(1, 0, 0, .7f);

        labelColor = labelText.color;

    }



    

    void Update() {
        // textGameobject.transform.position = m_Camera.WorldToViewportPoint(transform.position + labelOffset);
        transform.LookAt(transform.position + mainCamera.transform.rotation * Vector3.forward,
             mainCamera.transform.rotation * Vector3.up);
        float cameraDistance = Vector3.Distance(mainCamera.transform.position, transform.position);
         transform.localScale = new Vector3(cameraDistance / 200, cameraDistance / 200, cameraDistance / 200);
      //  labelText.color = labelColor - new Color(0, 0, 0, .5f);
    }

}
