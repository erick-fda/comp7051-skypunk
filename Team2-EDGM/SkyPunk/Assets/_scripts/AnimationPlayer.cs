﻿using UnityEngine;
using System.Collections;

public class AnimationPlayer : MonoBehaviour {
    public Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        anim.StartPlayback();
    }


}
