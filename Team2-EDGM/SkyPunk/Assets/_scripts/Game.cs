﻿/*===========================================================================================
    Game                                                                         *//**
	
	Manages game constants, state, and save data.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
*//*=======================================================================================*/

/*===========================================================================================
	Dependencies
===========================================================================================*/
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

/*===========================================================================================
	Game
===========================================================================================*/
/**
	Stores game constants and save data.
*/
public sealed class Game : MonoBehaviour
{
    /*=======================================================================================
		Singleton
	=======================================================================================*/
    /**
        Private static Game instance.
    */
    private static Game instance;

    /**
        Public access property for Game.instance.
        Private setting only.

        @see Game.instance
    */
    public static Game Instance
    {
        get { return instance; }

        private set { instance = value; }
    }

    /*=======================================================================================
		Fields
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		Game Object References
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Public
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Private
	---------------------------------------------------------------------------------------*/
    private SaveData data;

    /*=======================================================================================
		Properties
	=======================================================================================*/
    /**
        Public access property for Game.data.
        Private setting only.

        @see Game.data
    */
    public SaveData Data
    {
        get { return data; }

        private set { data = value; }
    }

    /*=======================================================================================
		Methods
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		MonoBehaviour Methods
	---------------------------------------------------------------------------------------*/
    /**
        Called once when the script is loaded.
        Used for the initialization of this object. Do not acces other objects in this 
        method, as they may not yet have been initialized.
        
        Enforces the singleton pattern on the Game.
    */
    void Awake()
    {
        /* Enforce the singleton pattern on the Game. */
        EnforceSingleton();
    }

    /*---------------------------------------------------------------------------------------
		Utility Methods
	---------------------------------------------------------------------------------------*/
    /**
        Initializes Game.data.

        If save data exists, loads it.
        If save data does not exist, creates a new empty save data object.
    */
    private void InitSaveData()
    {
        /*  If save data exists, load it. */
        if (File.Exists(Application.persistentDataPath + Refs.Filepaths.SaveDataFilepath))
        {
            /* Create a binary formatter to read the file. */
            BinaryFormatter binFormatter = new BinaryFormatter();

            /* Open a filestream to read the file. */
            FileStream filestream = File.Open(Application.persistentDataPath + Refs.Filepaths.SaveDataFilepath,
                FileMode.Open, FileAccess.Read);

            /* Set the game's data from the file. */
            Instance.Data = (SaveData)binFormatter.Deserialize(filestream);

            /* Close the filestream. */
            filestream.Close();
        }
        /* If no save data exists, create a new SaveData object for the Game instance. */
        else
        {
            Instance.Data = new SaveData();
        }
    }

    /**
        Load the specified scene.
        
        To account for cases where the game is paused at the time the next scene is loaded, 
        this method sets Time.timeScale to 1.

        @param scene - The name of the scene to load. Reference Refs.Scenes for convenience.
    */
    public void LoadScene(string scene)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

    /**
        Save Game.data to a save file.
    */
    public void SaveData()
    {
        /* If the save data directory does not exist, create it. */
        if (!Directory.Exists(Refs.Directories.SaveDataDirectory))
        {
            Directory.CreateDirectory(Application.persistentDataPath + Refs.Directories.SaveDataDirectory);
        }

        /* Create a binary formatter to save the file. */
        BinaryFormatter binFormatter = new BinaryFormatter();

        /* Open a filestream to save the file. */
        FileStream filestream = File.Open(Application.persistentDataPath + Refs.Filepaths.SaveDataFilepath,
            FileMode.OpenOrCreate);

        /* Save the game data to the file. */
        binFormatter.Serialize(filestream, Instance.Data);

        /* Close the filestream. */
        filestream.Close();
    }

    /**
        Sets the number of subsequent players.

        @param subsequentPlayers - The number of subsequent players to set.
    */
    public void SetSubsequentPlayers(int subsequentPlayers)
    {
        /* If numPlayers is less than 0, set it to 0.
            If it is greater than 3, set it to 3. */
        if (subsequentPlayers < 0)
        {
            subsequentPlayers = 0;
        }
        else if (subsequentPlayers > 3)
        {
            subsequentPlayers = 3;
        }

        /* Set the number of players in PlayerPrefs. */
        PlayerPrefs.SetInt(Refs.PlayerPrefs.SubsequentPlayers, subsequentPlayers);
    }
        
    /**
        Quits the game.
    */
    public void Quit()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    /*---------------------------------------------------------------------------------------
		Singleton Methods
	---------------------------------------------------------------------------------------*/
    /**
        Enforces singleton behaviour on the Game.
        Prevents secondary instances from being created and destroys them if they are.
    */
    private void EnforceSingleton()
    {
        /* If instance is null, set this Game as the instance and ensure that it is not 
            destroyed when loading subsequent scenes. Then, initialize game data. */
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);

            /* Initialize save data. */
            InitSaveData();
        }
        /* If instance is not null and is not this Game, destroy this Game. */
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
}
