﻿/*===========================================================================================
    Console                                                                             *//**
	
	Accepts console command inputs to modify game variables and states.
    Pauses the game when open.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.2.0
	@file
	
*//*=======================================================================================*/

/*===========================================================================================
	Dependencies
===========================================================================================*/
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*===========================================================================================
	Console
===========================================================================================*/
/**
	Accepts console command inputs to modify game variables and states.
    Pauses the game when open.
*/
public class Console : MonoBehaviour
{
    /*=======================================================================================
		Fields
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		Game Object References
	---------------------------------------------------------------------------------------*/
    public GameObject BattleSceneObject;    /**< The scene object for the scene. */
    public GameObject Window;               /**< The console window. */
    public InputField CommandInput;         /**< The input field. */
    public Text Log;                        /**< The command history log. */

    /*---------------------------------------------------------------------------------------
		Public
	---------------------------------------------------------------------------------------*/
    public const string SYSTEM_MESSAGE_PREFIX = ">> ";  /**< The prefix for console 
                                                                responses. */

    /*---------------------------------------------------------------------------------------
		Private
	---------------------------------------------------------------------------------------*/
    private BattleScene BattleSceneScript;  /**< The scene script for the scene. */
    private string Command = "";            /**< The last user command entered. */

    /*---------------------------------------------------------------------------------------
		Protected
	---------------------------------------------------------------------------------------*/


    /*=======================================================================================
		Properties
	=======================================================================================*/


    /*=======================================================================================
		Methods
	=======================================================================================*/
    /**
        Called once when the script is loaded.

        Used for setting up references to other scripts and enforcing singleton behaviours 
        on MonoBehaviours.
    */
    void Awake()
    {

    }

    /**
		Called once before the first frame update.
		Used for initializing referenced object variables and other values.

        Sets the sceneScript.
        Initializes the console window as closed and the command log as empty.
	*/
    void Start()
    {
        /* Set the sceneScript. */
        BattleSceneScript = BattleSceneObject.GetComponent<BattleScene>();

        /* Initialize console window as closed and command log as empty. */
        Window.SetActive(false);
        Log.text = "";
    }

    /**
		Called once per frame.
		Used for general updating of game values and objects.

        Can open and close the console.
        Can get input commands.
        Can fetch the last command entered. 
	*/
    void Update()
    {
        /* Open and close the console if input received. */
        Toggle();

        /* Execute command if input received. */
        CIn();

        /* Fetch last command if input received. */
        FetchLastCommand();
    }

    /**
		Called by a frame-rate-independent timer before game physics are updated.
		Used for calculating game physics.
	*/
    void FixedUpdate()
    {

    }

    /**
		Called after all Update() calculations have been performed.
		Used for follow cameras, procedural animation, and obtaining last known states.
	*/
    void LateUpdate()
    {

    }

    /*---------------------------------------------------------------------------------------
		Utility Methods
	---------------------------------------------------------------------------------------*/
    /**
        Tries to execute the command in the console's input field if input received.
    */
    private void CIn()
    {
        if (IRefs.GetKeyDown(IRefs.Command.EnterCommand))
        {
            /* Log what command was entered.*/
            Debug.Log("Command \"" + CommandInput.text + "\" entered into console.");

            /* Take the command, copy it to the log, clear the input field, and return the 
                focus to the input field. */
            Command = CommandInput.text;
            CommandInput.text = "";
            COut(Command);
            SetFocus();

            /* Split the command into words. */
            string[] commandTerms = Command.ToLower().Split(' ');

            /* If the command is valid, pass its parameters on to the correct method. 
                Otherwise, print a line to the log saying the command is invalid. */
            switch (commandTerms[0])
            {
                case "quit":
                    Quit();
                    break;
                case "show":
                    Show(commandTerms);
                    break;
                case "to":
                    To(commandTerms);
                    break;
                default:
                    COut("\"" + Command + "\" is not a valid command.", true);
                    break;
            }
        }
    }

    /**
        Prints a message to the console's log.

        @param message - The message to log.
        @param flagAsSystem - Whether the printed message should be flagged as a system 
                                message. Defaults to false.
    */
    private void COut(string message, bool flagAsSystem = false)
    {
        /* Log the message to the console log, flagging it as a system message if 
            appropriate. */
        Log.text += (flagAsSystem ? SYSTEM_MESSAGE_PREFIX + " " : "") + message + "\n";
    }

    /**
        Fill the console's input field with the previous command entered if input received.
        Also set the cursor focus on the console's input field.
    */
    private void FetchLastCommand()
    {
        if (IRefs.GetKeyDown(IRefs.Command.FetchLastCommand))
        {
            CommandInput.text = Command;
            SetFocus();
        }
    }

    /**
        Sets the cursor focus on the console's input field.
    */
    private void SetFocus()
    {
        CommandInput.ActivateInputField();
        CommandInput.Select();
    }

    /**
        Handles a console "show" command.

        @param commandTerms - An array containing the words entered to call the command.
    */
    private void Show(string[] commandTerms)
    {
        /* If no parameters have been provided for the show command, print a message to the 
            log saying so and return. */
        if (commandTerms.Length < 2)
        {
            COut("Command \"show\" requires at least one parameter.", true);
            return;
        }

        /* Executes the appropriate show command depending on the command terms. */
        switch (commandTerms[1])
        {
            case "leaderboard":
                ShowLeaderboard();
                break;
            case "teamscores":
                ShowTeamScores();
                break;
            default:
                COut("\"" + commandTerms[1] + "\" is not a valid parameter for command " + 
                    "\"show\".", true);
                break;
        }
    }

    /**
        Prints the game's leaderboard to the console log.
    */
    private void ShowLeaderboard()
    {
        /* Sort the leaderboard. */
        Game.Instance.Data.Leaderboard.Sort();

        /* Print a heading. */
        COut("\n----- LEADERBOARD -----");

        /* Print the scores on the leaderboard in descending order. */
        for (int i = (Game.Instance.Data.Leaderboard.Count - 1); i >= 0; i--)
        {
            /* If the score is equal to the minimum score, display it as a string of 
                hyphens. */
            if (Game.Instance.Data.Leaderboard[i] == Refs.Scoring.MinimumTeamScore)
            {
                COut("---");
            }
            /* If the score is greater than the minimum score, display it. */
            else
            {
                COut(Game.Instance.Data.Leaderboard[i].ToString());
            }
        }

        /* Print an empty line. */
        COut("");
    }

    /**
        Prints the current team scores to the console log.
    */
    private void ShowTeamScores()
    {
        /* Sort the leaderboard. */
        Game.Instance.Data.Leaderboard.Sort();

        /* Print a heading. */
        COut("\n----- SCORES -----");

        /* Print the team scores. */
        COut("Player Team Score: " + BattleSceneScript.TeamScores[1]);
        COut("Enemy Team Score: " + BattleSceneScript.TeamScores[0]);

        /* Print an empty line. */
        COut("");
    }

    /**
        Navigates between game scenes.

        @param commandTerms - An array containing the words entered to call the command.
    */
    private void To(string[] commandTerms)
    {
        /* If no parameters have been provided for the to command, print a message to the 
            log saying so and return. */
        if (commandTerms.Length < 2)
        {
            COut("Command \"to\" requires at least one parameter.", true);
            return;
        }

        /* Executes the appropriate to command depending on the command terms. */
        switch (commandTerms[1])
        {
            case "mainmenu":
                ToMainMenu();
                break;
            default:
                COut("\"" + commandTerms[1] + "\" is not a valid parameter for command " +
                    "\"to\".", true);
                break;
        }
    }

    /**
        Loads the main menu scene.
    */
    private void ToMainMenu()
    {
        SceneManager.LoadScene(Refs.Scenes.MainMenu);
    }

    /**
        Opens and closes the console window.
        Pauses the game when the console is open and unpauses when the console is closed.
    */
    private void Toggle()
    {
        if (IRefs.GetKeyDown(IRefs.Command.ToggleConsole))
        {
            Window.SetActive(!Window.activeSelf);
            BattleSceneScript.Paused = Window.activeSelf;
            SetFocus();
        }
    }

    /**
        Quits the game.
    */
    private void Quit()
    {
        Game.Instance.Quit();
    }
}
