﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

	public Projectile projectile;

	public Transform spawnPoint;

    public float reloadTime = .2f;

    public float spread = 1;

    public AudioSource firingSound;

    public ParticleSystem firingEffect;

    /**is the projectile a particle system */
    public bool isParticleSystem = false;

    private float timeSinceFired;

    // Use this for initialization
    void Start () {
        timeSinceFired = reloadTime;
	}
	
	// Update is called once per frame
	void Update () {
        timeSinceFired += Time.deltaTime;
        if(isParticleSystem && timeSinceFired >= reloadTime) {
            projectile.GetComponent<ParticleSystem>().loop = false;
            //projectile.GetComponent<ParticleSystem>().Stop();
        }
            

    }

	//Instantiates a bullet at the spawn point
	public void fire() {
        if (isParticleSystem) {
            timeSinceFired = 0;
            projectile.GetComponent<ParticleSystem>().loop = true;
            if (!projectile.GetComponent<ParticleSystem>().isPlaying)
                projectile.GetComponent<ParticleSystem>().Play();

            if (firingSound != null && !firingSound.isPlaying)
                firingSound.Play();
        }
        else if (timeSinceFired >= reloadTime)
        {
            timeSinceFired = 0;
            Projectile p = Instantiate(projectile) as Projectile;
            p.transform.position = spawnPoint.position;
            float xDegrees = spawnPoint.eulerAngles.x + Random.Range(-spread, spread);
            float yDegrees = spawnPoint.eulerAngles.y + Random.Range(-spread, spread);
            float zDegrees = spawnPoint.eulerAngles.z + Random.Range(-spread, spread);
            p.transform.rotation = Quaternion.Euler(xDegrees, yDegrees, zDegrees);
            p.GetComponent<Rigidbody>().velocity = p.transform.forward * p.GetComponent<Projectile>().velocity;

            if (firingEffect != null) 
                firingEffect.Play();

            if (firingSound != null && !firingSound.isPlaying)
                firingSound.Play();
        }

    }
}
