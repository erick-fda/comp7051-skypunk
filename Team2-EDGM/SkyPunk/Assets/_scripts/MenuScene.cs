﻿/*===========================================================================================
    MenuScene                                                                           *//**
	
	Manages a main menu scene.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
*//*=======================================================================================*/

/*===========================================================================================
	Dependencies
===========================================================================================*/
using UnityEngine;
using UnityEngine.UI;

/*===========================================================================================
	MenuScene
===========================================================================================*/
/**
	Manages a main menu scene.
*/
public class MenuScene : MonoBehaviour
{
    /*=======================================================================================
		Internal Types
	=======================================================================================*/
    public struct MenuViewNames
    {
        public const string MenuView = "MenuView";
        public const string LeaderboardView = "LeaderboardView";
        public const string InstructionsView = "InstructionsView";
    }

    /*=======================================================================================
		Fields
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		Game Object References
	---------------------------------------------------------------------------------------*/
    public GameObject MenuView;         /**< The menu screen which shows menu buttons. */
    public GameObject LeaderboardView;  /**< The menu screen which shows the leaderboard. */
    public GameObject InstructionsView; /**< The menu screen which shows game instructions. */
    public Text LeaderboardDisplay;     /**< The leaderboard display. */

    /*---------------------------------------------------------------------------------------
		Public
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Private
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Protected
	---------------------------------------------------------------------------------------*/


    /*=======================================================================================
		Properties
	=======================================================================================*/

    /*=======================================================================================
		Methods
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		MonoBehaviour Methods
	---------------------------------------------------------------------------------------*/
    /**
        Called once when the script is loaded.

        Used for setting up references to other scripts and enforcing singleton behaviours 
        on MonoBehaviours.
    */
    void Awake()
    {

    }

    /**
		Called once before the first frame update.
		Used for initializing referenced object variables and other values.
        
        Sets the text on the leaderboard display.
	*/
    void Start()
    {
        /* Set the text on the leaderboard display. */
        SetLeaderboardDisplay();
    }

    /**
		Called once per frame.
		Used for general updating of game values and objects.
	*/
    void Update()
    {

    }

    /**
		Called by a frame-rate-independent timer before game physics are updated.
		Used for calculating game physics.
	*/
    void FixedUpdate()
    {

    }

    /**
		Called after all Update() calculations have been performed.
		Used for follow cameras, procedural animation, and obtaining last known states.
	*/
    void LateUpdate()
    {

    }

    /*---------------------------------------------------------------------------------------
		Utility Methods
	---------------------------------------------------------------------------------------*/
    /**
        Sets the text on the leaderboard display.
    */
    private void SetLeaderboardDisplay()
    {
        /* Add a heading to the display string. */
        string displayText = "----- LEADERBOARD -----\n";

        /* Add each score to the display string in descending order. */
        Game.Instance.Data.Leaderboard.Sort();
        for (int i = (Game.Instance.Data.Leaderboard.Count - 1); i >= 0; i--)
        {
            /* If the score is equal to the minimum score, display it as a string of 
                hyphens. */
            if (Game.Instance.Data.Leaderboard[i] == Refs.Scoring.MinimumTeamScore)
            {
                displayText += "\n---";
            }
            /* If the score is greater than the minimum score, display it. */
            else
            {
                displayText += "\n" + Game.Instance.Data.Leaderboard[i];
            }
        }

        /* Set the display string on the display. */
        LeaderboardDisplay.text = displayText;
    }

    /*---------------------------------------------------------------------------------------
		Game Methods
	---------------------------------------------------------------------------------------*/
    /**
        Loads the specified scene.

        @param scene - The name of the scene to load.
    */
    public void LoadScene(string scene)
    {
        Game.Instance.LoadScene(scene);
    }

    /**
        Quits the game.
    */
    public void Quit()
    {
        Game.Instance.Quit();
    }

    /**
        Toggle main menu view between main view and leaderboard view.
    */
    public void SetMenuView(string viewName)
    {
        switch (viewName)
        {
            case MenuViewNames.MenuView:
                MenuView.SetActive(true);
                LeaderboardView.SetActive(false);
                InstructionsView.SetActive(false);
                break;

            case MenuViewNames.LeaderboardView:
                MenuView.SetActive(false);
                LeaderboardView.SetActive(true);
                InstructionsView.SetActive(false);
                break;

            case MenuViewNames.InstructionsView:
                MenuView.SetActive(false);
                LeaderboardView.SetActive(false);
                InstructionsView.SetActive(true);
                break;

            default:
                Debug.Log("Invalid menu view \"" + viewName + "\" requested.");
                break;
        }
    }

    /**
        Sets the number of subsequent players.

        @param subsequentPlayers - The number of subsequent players to set.
    */
    public void SetSubsequentPlayers(int subsequentPlayers)
    {
        Game.Instance.SetSubsequentPlayers(subsequentPlayers);
    }
}
