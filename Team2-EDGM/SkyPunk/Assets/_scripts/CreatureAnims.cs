﻿using UnityEngine;
using System.Collections;

public class CreatureAnims : MonoBehaviour {
    public Animator anim;

    private float descentAngle = 0;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {   
        

       float angle = Vector3.Angle(Vector3.up, transform.forward);
        angle -= 75;
        angle /= -30;
        descentAngle = angle;
       
        anim.SetFloat("descentAngle", descentAngle);


    }
}
