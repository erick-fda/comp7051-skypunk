﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Effects;

public class Destructible : MonoBehaviour {

	public float maxHealth = 100;

	public float currentHealth = 100;
    public RectTransform healthBar;

    public float scoreValue = 10;

	public GameObject wreckage;

	// 0 = world, 1 = Team1, 2 = Team2
	public int team;

	//player,AIShip,turret (possibly change to enumerated type)
	public string type;

    //variables created for the destruction
    public float duration = 5.0f;
    public GameObject smoke;
    public GameObject explosion;
    public GameObject fire;
    public GameObject explosion2;
	public float respawnTime = 3;

    //created to control destruction states 
    bool isDamaged = false;
    bool isAlive = true;

    /* The scene object for the scene. */
    private GameObject BattleSceneObject;
    private BattleScene BattleSceneScript;

    // Use this for initialization
    void Start ()
    {
        BattleSceneObject = GameObject.Find("Battle Scene");
        BattleSceneScript = BattleSceneObject.GetComponent<BattleScene>();
        if (tag == Refs.Tags.Player)
            healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	public void damage(float damageAmount) {
        currentHealth -= damageAmount;

        if (tag == Refs.Tags.Player)
            healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);

        if ((currentHealth <= maxHealth/2) && (!isDamaged) && (tag != Refs.Tags.Player)) {
            isDamaged = true;
            GameObject smk = Instantiate(smoke, transform.position, transform.rotation) as GameObject;
            smk.transform.parent = gameObject.transform;
            smk.SetActive(true);
        }

        if (currentHealth <= 0 && isAlive){
            isAlive = false;
            
            StartCoroutine(Destruction());
        }
    }

	public void setHealth(float newHealth) {

	}

    IEnumerator Destruction() {
        /* If the Destructible is not a player, make it explode. */
        if (tag != Refs.Tags.Player)
        {
            GameObject exp1 = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
            exp1.SetActive(true);
        }
        //     exp1.GetComponent<ExplosionPhysicsForce>().explosionForce = 10.0f;
        //if ((type == "Air") && (tag != Refs.Tag[Refs.TagName.Player])) {
        if ((tag != Refs.Tags.Player)) {
            if(type == "Air") {
                gameObject.GetComponent<Rigidbody>().drag = .3f;
                gameObject.GetComponent<Rigidbody>().angularDrag = .1f;
                gameObject.GetComponent<Rigidbody>().useGravity = true;
            }
            yield return new WaitForSeconds(duration / 2);
            GameObject fir = Instantiate(fire, transform.position, transform.rotation) as GameObject;
            fir.transform.parent = gameObject.transform;
            fir.SetActive(true);
            AI aiScript = gameObject.GetComponent<AI>();
            if (gameObject.GetComponent<DontFlyAway>() != null)
                gameObject.GetComponent<DontFlyAway>().enabled = false;
            if (aiScript != null)
                aiScript.enabled = false;
            PlayerController playerScript = gameObject.GetComponent<PlayerController>();
            if (playerScript != null)
                playerScript.enabled = false;
            yield return new WaitForSeconds(duration);
            GameObject exp2 = Instantiate(explosion2, transform.position, transform.rotation) as GameObject;
            exp2.GetComponent<ParticleSystem>().Play();
            yield return new WaitForSeconds(.9f);
            Destroy(gameObject);
        }

        /* If the Destructible is a player, respawn them. */
        if (tag == Refs.Tags.Player)
        {
			BattleSceneScript.PlayerDeath(gameObject, (int)-scoreValue, respawnTime, true);
        }
        else if (team == 1)
        {
            BattleSceneScript.ScoreBothTeams(1, (int)-scoreValue);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.GetComponent<Projectile>())
        {
            damage(col.transform.GetComponent<Projectile>().damage);
        }
    }

    void OnParticleCollision(GameObject col) {
        if (col.transform.GetComponent<Projectile>()) {
            damage(col.transform.GetComponent<Projectile>().damage);
        }
    }

}
