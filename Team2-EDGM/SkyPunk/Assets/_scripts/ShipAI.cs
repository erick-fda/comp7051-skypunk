﻿using UnityEngine;
using System.Collections;


/**
Governs AI controlled aircraft behaviour
**/
public class ShipAI : AI {
    //how fast the ship can go
    public float topSpeed = 25.0f;

    //how fast the ship can turn
    public float turnSpeed = 1.0f;

    /**how much faster than normal the aircraft will turn to avoid collision*/
    public float maneuverSpeedMultiplier = 1.5f;

    /** how much the aircraft tries to avoid being uoside down*/
    public float rightingSpeed = .2f;

    // range at which to detect potential obstacles
    public float avoidanceDistance = 20.0f;

    //how many seconds until reingaging target after obstacle avoidance. 
    public float overSteer = 2;

    //how much bigger the raycasts are than the objects hitbox.
    public float sensorMargin = 1.0f;


    // how long since the last collision avoidance
    private float timeSinceAvoidance = 100.0f;

    //the collider of the AI ship (Must be a box collider!)
    private BoxCollider ownCollider;

    //direction of the closest obstacle
    private Vector3 closestContactDir = new Vector3(1, 0, 0); 

    //the gameobject's rigidbody component
    private Rigidbody rb;

    //how quickly the aircraft accelerates
    private float thrust = 300;

    //the distance of the closest obstacle detected
    private float minDistance = 9999.0f;

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody>();

        shoot = GetComponent<Shoot>();
        if (shoot == null)
            Debug.Log("No Shoot script found on " + this.gameObject.name);

        ownCollider = GetComponent<BoxCollider>();
        if (ownCollider == null)
            Debug.Log("No BoxCollider found on " + gameObject.name);

        InvokeRepeating("acquireTarget", 2, 3F);
    }

    void FixedUpdate() {
        //move the ship forward
        if (Vector3.Project(rb.velocity, transform.forward).magnitude < topSpeed)
            rb.AddForce(transform.forward * thrust, ForceMode.Acceleration);

        //updatecollision avoidance timer
        if (timeSinceAvoidance <= overSteer)
            timeSinceAvoidance += Time.deltaTime;

        if (target == null) {
            rb.AddTorque(transform.up * turnSpeed * .6f);
            acquireTarget();
        }
        

        mainMovementController();
        rollUpright();
    }

    /**
        Points the ship towards the target.
        If intervening obstacle detected, attempts to maneuver around it.
        If within range an has a clear line of sight, open fire on the target.
    **/
    private void attackTarget() {
        if(target == null)
            return;

        Vector3 maneuverDir = target.position - transform.position;
        Vector3 targetDir = target.position - transform.position;
        float targetAngle = Vector3.Angle(targetDir, transform.forward);

        //if target in range and in sight shoot, if in range but not in sight maneuver to avoid obstacle
        RaycastHit hit;
        if (Physics.Raycast(transform.position, targetDir, out hit, attackRange)) {

            if (hit.transform != target.transform && hit.transform.tag != "Projectile") {
                Quaternion quat = Quaternion.AngleAxis(70, new Vector3(1, 0, 0));
                maneuverDir = quat * (target.position - transform.position);
            }
            else if (targetAngle < 25.0f && hit.transform == target.transform)
                shoot.fire();
        }
        rb.AddTorque(Vector3.Cross(transform.forward, maneuverDir).normalized * turnSpeed);
    }

    /**
    checks if there are collision risks. If Collision risk detected attempts to avoid. if no collision detected
    attacks current target by calling attack target method
    **/
    private void mainMovementController() {
        //sets the direction for the raycast "sensors"
        Quaternion topQuat = Quaternion.AngleAxis(-10, transform.right);
        Vector3 topDir = topQuat * transform.forward * 30f;

        Quaternion rightQuat = Quaternion.AngleAxis(10, transform.up);
        Vector3 rightDir = rightQuat * transform.forward * 30f;

        Quaternion bottomQuat = Quaternion.AngleAxis(10, transform.right);
        Vector3 bottomDir = bottomQuat * transform.forward * 30f;

        Quaternion leftQuat = Quaternion.AngleAxis(-10, transform.up);
        Vector3 leftDir = leftQuat * transform.forward * 30f;

        Vector3[] sensorDirs = { topDir, rightDir, bottomDir, leftDir };

        //sets the starting positions for sensor raycasts
        Vector3 topOrigin = transform.TransformPoint(Vector3.up * (ownCollider.size.y / 2 + sensorMargin));
        Vector3 rightOrigin = transform.TransformPoint(Vector3.right * (ownCollider.size.x / 2 + sensorMargin));
        Vector3 bottomOrigin = transform.TransformPoint(Vector3.down * (ownCollider.size.y / 2 + sensorMargin));
        Vector3 leftOrigin = transform.TransformPoint(Vector3.left * (ownCollider.size.x / 2 + sensorMargin));

        Vector3[] sensorOrigins = { topOrigin, rightOrigin, bottomOrigin, leftOrigin };

        //loops through the 4 side raycasts, if obstacle detected sets the closest one to obstacle to determine evation direction
        RaycastHit hit;
        RaycastHit[] hits;
        minDistance = 9999.0f;
        for (int i = 0; i < 4; i++) {
            hits = (Physics.RaycastAll(sensorOrigins[i], sensorDirs[i], avoidanceDistance));
            for (int j = 0; j < hits.Length; j++) {
                hit = hits[j];
                if (hit.collider.tag != "Projectile") {
                    Vector3 hitVelocity = new Vector3();
                    if (hit.rigidbody != null)
                        hitVelocity = hit.rigidbody.velocity;

                    float relativeVelocity = (hitVelocity - rb.velocity).magnitude;
                    if (hit.distance < (relativeVelocity / topSpeed) * avoidanceDistance)
                        timeSinceAvoidance = 0;

                    if (hit.distance < minDistance) {
                        closestContactDir = sensorOrigins[i];
                        minDistance = hit.distance;
                    }
                }
            }  
        }

        //creates the central sensor raycast

        if (Physics.Raycast(transform.position, transform.forward, out hit, avoidanceDistance * 1.1f)) {
            
            if (hit.collider.tag != "Projectile") {
                Vector3 hitVelocity = new Vector3();
                if (hit.rigidbody != null)
                    hitVelocity = hit.rigidbody.velocity;

                float relativeVelocity = (hitVelocity - rb.velocity).magnitude;
                if (hit.distance < (relativeVelocity / topSpeed) * avoidanceDistance)
                    timeSinceAvoidance = 0;
            }   
        }

        if (timeSinceAvoidance < overSteer) {
            avoidCollision(sensorOrigins);
        }
        else
            attackTarget();

        //draws bounding sensor raycasts for debugging
        Debug.DrawRay(topOrigin, sensorDirs[0], Color.red);
        Debug.DrawRay(rightOrigin, sensorDirs[1], Color.red);
        Debug.DrawRay(bottomOrigin, sensorDirs[2], Color.red);
        Debug.DrawRay(leftOrigin, sensorDirs[3], Color.red);
        Debug.DrawRay(transform.position, transform.forward * avoidanceDistance * 1.2f, Color.red);
    }

    /**
        Creates 4 more raycasts to act as sensors to determine which direction it is safest to maneuver towards,
        Then turns the ship in that direction to avoid obstacles
    **/
    private void avoidCollision(Vector3[] sensorOrigins) {
        Quaternion topQuat = Quaternion.AngleAxis(-25, transform.right);
        Vector3 topDir = topQuat * transform.forward * avoidanceDistance;

        Quaternion rightQuat = Quaternion.AngleAxis(25, transform.up);
        Vector3 rightDir = rightQuat * transform.forward * avoidanceDistance;

        Quaternion bottomQuat = Quaternion.AngleAxis(25, transform.right);
        Vector3 bottomDir = bottomQuat * transform.forward * avoidanceDistance;

        Quaternion leftQuat = Quaternion.AngleAxis(-25, transform.up);
        Vector3 leftDir = leftQuat * transform.forward * avoidanceDistance;

        Vector3[] sensorDirs = { topDir, rightDir, bottomDir, leftDir };

        //loops through each of the 4 vectors, creating raycasts and checking which one is free of obstacles
        RaycastHit hit;
        RaycastHit[] hits;
       // float minDistance = 9999.0f;
        for (int i = 0; i < sensorOrigins.Length; i++) {
            hits = Physics.RaycastAll(sensorOrigins[i], sensorDirs[i], avoidanceDistance);
            for (int j = 0; j < hits.Length; j++) {
                hit = hits[j];
                
                if (hit.collider.tag != "Projectile") {
                    if (hit.distance < minDistance) {
                        closestContactDir = sensorOrigins[i];
                        minDistance = hit.distance;
                    }
                } 
            }
        }

        //draws "wide angle" sensor raycasts for debugging purposes
        Debug.DrawRay(sensorOrigins[0], topDir, Color.green);
        Debug.DrawRay(sensorOrigins[1], rightDir, Color.green);
        Debug.DrawRay(sensorOrigins[2], bottomDir, Color.green);
        Debug.DrawRay(sensorOrigins[3], leftDir, Color.green);

        //turn the ship towards targetDir
        Vector3 targetDir = transform.position - closestContactDir;
        float maneuverSpeed = turnSpeed;
        if(minDistance < avoidanceDistance / 2)
            maneuverSpeed *= maneuverSpeedMultiplier;
        rb.AddTorque(Vector3.Cross(transform.forward, targetDir).normalized * maneuverSpeed);
    }

    /**
    roll the aircraft so that it is right side up
    **/
    private void rollUpright() {
        float step = turnSpeed * rightingSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.up, Vector3.up, step, 0.0F);
        transform.rotation = Quaternion.LookRotation(transform.forward, newDir);
    }   


}
