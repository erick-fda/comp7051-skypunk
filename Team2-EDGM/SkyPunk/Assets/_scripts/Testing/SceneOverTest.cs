﻿/*===========================================================================================
    SceneOverTest                                                                            *//**
	
	Ends the scene a certain amount of time after the scene starts.
    Used to test scene end behaviour.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
*//*=======================================================================================*/

/*===========================================================================================
	Dependencies
===========================================================================================*/
using System.Collections;
using UnityEngine;

/*===========================================================================================
	SceneOverTest
===========================================================================================*/
/**
	Ends the scene a certain amount of time after the scene starts.
    Used to test scene end behaviour.
*/
public class SceneOverTest : MonoBehaviour
{
    /*=======================================================================================
		Fields
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		Game Object References
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Public
	---------------------------------------------------------------------------------------*/
    public float SceneEndDelay = 3; /**< Seconds to wait before ending the scene. */

    /*---------------------------------------------------------------------------------------
		Private
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Protected
	---------------------------------------------------------------------------------------*/


    /*=======================================================================================
		Properties
	=======================================================================================*/


    /*=======================================================================================
		Methods
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		MonoBehaviour Methods
	---------------------------------------------------------------------------------------*/
    /**
        Called once when the script is loaded.

        Used for setting up references to other scripts and enforcing singleton behaviours 
        on MonoBehaviours.
    */
    void Awake()
    {

    }

    /**
		Called once before the first frame update.
		Used for initializing referenced object variables and other values.

        Ends the scene a certain amount of time after the scene starts.
	*/
    void Start()
    {
        StartCoroutine(EndScene());
    }

    /**
		Called once per frame.
		Used for general updating of game values and objects.
	*/
    void Update()
    {

    }

    /**
		Called by a frame-rate-independent timer before game physics are updated.
		Used for calculating game physics.
	*/
    void FixedUpdate()
    {

    }

    /**
		Called after all Update() calculations have been performed.
		Used for follow cameras, procedural animation, and obtaining last known states.
	*/
    void LateUpdate()
    {

    }

    /*---------------------------------------------------------------------------------------
		Utility Methods
	---------------------------------------------------------------------------------------*/
    /**
        Ends the scene using the Scene.SceneOver() method after a delay.

        @returns An IEnumerator of the delay before ending the scene.
    */
    private IEnumerator EndScene()
    {
        /* Wait for SceneEndDelay seconds. */
        yield return new WaitForSeconds(SceneEndDelay);

        /* End the scene. */
        GameObject.Find("Scene").GetComponent<BattleScene>().EndRound();
    }
}
