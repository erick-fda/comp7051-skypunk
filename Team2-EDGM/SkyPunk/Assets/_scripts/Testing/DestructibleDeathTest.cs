﻿/*===========================================================================================
    DestructibleDeathTest                                                                          *//**
	
	Kills a Destructible object a certain amount of time after the scene starts.
    Used to test Destructible death and respawn events.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
*//*=======================================================================================*/

/*===========================================================================================
	Dependencies
===========================================================================================*/
using System.Collections;
using UnityEngine;

/*===========================================================================================
	DestructibleDeathTest
===========================================================================================*/
/**
	Kills a Destructible object a certain amount of time after the scene starts.
    Used to test Destructible death and respawn events.
*/
public class DestructibleDeathTest : MonoBehaviour
{
    /*=======================================================================================
		Fields
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		Game Object References
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Public
	---------------------------------------------------------------------------------------*/
    public float DeathDelay = 3;    /**< Seconds to wait before killing the Destructible. */
    
    /*---------------------------------------------------------------------------------------
		Private
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Protected
	---------------------------------------------------------------------------------------*/


    /*=======================================================================================
		Properties
	=======================================================================================*/


    /*=======================================================================================
		Methods
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		MonoBehaviour Methods
	---------------------------------------------------------------------------------------*/
    /**
        Called once when the script is loaded.

        Used for setting up references to other scripts and enforcing singleton behaviours 
        on MonoBehaviours.
    */
    void Awake()
    {

    }

    /**
		Called once before the first frame update.
		Used for initializing referenced object variables and other values.

        Kills the Destructible a certain amount of time after the scene starts.
	*/
    void Start()
    {
        StartCoroutine(Die());
    }

    /**
		Called once per frame.
		Used for general updating of game values and objects.
	*/
    void Update()
    {

    }

    /**
		Called by a frame-rate-independent timer before game physics are updated.
		Used for calculating game physics.
	*/
    void FixedUpdate()
    {

    }

    /**
		Called after all Update() calculations have been performed.
		Used for follow cameras, procedural animation, and obtaining last known states.
	*/
    void LateUpdate()
    {

    }

    /*---------------------------------------------------------------------------------------
		Utility Methods
	---------------------------------------------------------------------------------------*/
    /**
        Kills the Destructible using the Destructible.damage() method after a delay.

        @returns An IEnumerator of the delay before killing the Destructible.
    */
    private IEnumerator Die()
    {
        /* Wait for DeathDelay seconds. */
        yield return new WaitForSeconds(DeathDelay);

        /* Kill the Destructible. */
        GetComponent<Destructible>().damage(GetComponent<Destructible>().maxHealth);
    }
}
