﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

/** Keeps the ship from flying too far away from the battlefield.*/
public class DontFlyAway : MonoBehaviour {
    /**The height at which the maximum vertical force is applied to push the aircraft towards the center*/
    public static float verticalBoundaryEnd = 600;

    /**the height at which force begins to be applied to move the aircraft away from the boundary*/
    public static float verticalBoundaryStart = 200;

    /**The maximum vertical force that is applied to the object to push it towards the center*/
    public static float maxForceStrength = 500;

    /**The horizontal distance at which the "return to battlefield" warning is displayed*/
    public static float horizontalWarningDistance = 500;

    /**The horizontal boundary of the battlefield. At thgis distance the object will be destroyed.*/
    public static float horizontalBoundarySize = 700;

    /** the "return to battlefield" warning text*/
    public Text warningMessage;

    /**the object's rigidBody. Used for the vertical boundaries to push the object towards the center*/
    private Rigidbody rb;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        if (rb == null)
            Debug.Log("DontFlyAway class failed to get Rigidbody on " + gameObject.name);
	}

    void FixedUpdate() {
        //push the object to the center along the vertical axis. Pushes harder as object aproaches verticalBoundarySize 
        if(Math.Abs(transform.position.y) > verticalBoundaryStart){
            float forceScale = (transform.position.y - verticalBoundaryStart) / (verticalBoundaryEnd - verticalBoundaryStart) * maxForceStrength;
            rb.AddForce(-Vector3.up * forceScale);
        }
        
        
        //displays a warning if player is close to being out of bounds (horizontal boundary only)
        if (warningMessage != null
            && (Math.Abs(transform.position.x) > horizontalWarningDistance || Math.Abs(transform.position.y) > horizontalWarningDistance)) {
            warningMessage.gameObject.SetActive(true);
        }
        else if(warningMessage != null) {
            warningMessage.gameObject.SetActive(false);
        }

        //take damage if outside horizontal battlefield boundary
        if (Math.Abs(transform.position.x) > horizontalBoundarySize || Math.Abs(transform.position.y) > horizontalBoundarySize) {
            GetComponent<Destructible>().damage(5);
        }
    }
}
