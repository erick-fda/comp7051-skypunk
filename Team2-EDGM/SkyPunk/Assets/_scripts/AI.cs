﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour {
    //maximum range at which AI will open fire on a target
    public float attackRange = 100;

    //range at which a target can be acquired
    public float acquisitionRange = 200;

    //preffered type of target. will attack these targets first
    public string preferredTarget = "Air";

    //the target to attack
    public Transform target;

    //the Shoot script associated with this object
    protected Shoot shoot;

    //the aircrafts own Destructible Object
    protected Destructible destructible;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update () {
	
	}

    /**
    selects the most attractive target within acquisition range. If not target is within range,
    reverts to default behaviour
    @return bool - true if a suitable target was found
    **/
    protected bool acquireTarget() {
        float appealScore;
        float bestScore = 0;
        int ownTeam = GetComponent<Destructible>().team;
        Destructible bestTarget = null;
        Destructible[] targets = FindObjectsOfType(typeof(Destructible)) as Destructible[];
      //  Debug.Log(destructible.team);
        for (int i = 0; i < targets.Length; i++) {
            Vector3 targetDirection = targets[i].transform.position - transform.position;
            if (targets[i].team != ownTeam && targets[i].team != 0 && targetDirection.magnitude < acquisitionRange) {
                appealScore = 1000;
                appealScore -= targetDirection.magnitude - attackRange / 2;
                appealScore -= 2 * Vector3.Angle(targetDirection, transform.forward);
                if (System.String.Equals(targets[i].type, preferredTarget))
                    appealScore += 100;

                if (appealScore > bestScore) {
                    bestScore = appealScore;
                    bestTarget = targets[i];
                }
            }
        }

        if (bestScore > 0) {
            target = bestTarget.transform;
            return true;
        }
        else {
            target = null;
            return false;
        }
            
    }
}
