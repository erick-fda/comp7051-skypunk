﻿using UnityEngine;
using System.Collections;

public class TurretAI : AI {
    //the gun-barrel part of the turret. Can only turn up and down
    public Transform gunBarrel;

    //the base of the turret. can only turn side to side
    public Transform turretBase;


    //how fast the gun can elevate (up and down movement speed)
    public float elevationSpeed = 20.0f;

    //how fast the turret can turn (side to side movement)
    public float turnSpeed = 40.0f;


    //how far down the gun can point. 0 = horizontal,  -1 = straight down, 1 = straight up
    public float gunDepression = 0;

    // Use this for initialization
    void Start() {
        if (GetComponent<Shoot>() == null)
            Debug.Log(gameObject.name + ": turretAI needs to have a Shoot object associated with it");
        shoot = GetComponent<Shoot>();

        if (GetComponent<Destructible>() == null)
            Debug.Log(gameObject.name + ": turretAI needs to have a Shoot object associated with it");
        destructible = GetComponent<Destructible>();
    }

    // Update is called once per frame
    void Update() {

        if (target == null || (target.position - transform.position).magnitude > acquisitionRange) {
            target = null;
            acquireTarget();
        } else
            aim();
    }

    /** 
    Rotates the gun to aim at the target. Rotates the base of the turret horizontally and the barrel of the turret vertically
        **/ 
    private void aim() {
        Vector3 predictedPositition = leadTaget();
        Vector3 dirToTarget = gunBarrel.InverseTransformDirection(predictedPositition - gunBarrel.position);

        if (dirToTarget.normalized.x > .01f) {
            turretBase.RotateAround(turretBase.position, turretBase.up, Time.deltaTime * turnSpeed);
        }
        else if (dirToTarget.normalized.x < -.01f) {
            turretBase.RotateAround(turretBase.position, turretBase.up, Time.deltaTime * -turnSpeed);
        }

        float gunElevation = transform.InverseTransformDirection(predictedPositition - gunBarrel.position).normalized.y;
        if (gunElevation >= gunDepression) {
            if (dirToTarget.normalized.y > .01f) {
                gunBarrel.RotateAround(gunBarrel.position, gunBarrel.right, Time.deltaTime * -elevationSpeed);
            }

            else if (dirToTarget.normalized.y < -.01f) {
                gunBarrel.RotateAround(gunBarrel.position, gunBarrel.right, Time.deltaTime * elevationSpeed);
            }
        }

        //check if target is in range, if barrel is pointed at target, and if there is a clear line of sight. the shoots
        float range = Vector3.Distance(predictedPositition, gunBarrel.position);
        Vector3 attackDirection = predictedPositition - gunBarrel.position;
        float targetAngle = Vector3.Angle(attackDirection, gunBarrel.forward);
        if (range < attackRange && targetAngle < 4.0f) {
            if (!Physics.Raycast(gunBarrel.position, attackDirection, range - 10))
                shoot.fire();
       }
    }

    /**
        Calculates the target's position when the projectile will reach it, based on target's current velocity,
        and projectile speed. 
        Used to lead fast moving targets for increased accuracy.
        @return Vector3 the predicted position of the target
    **/
    public Vector3 leadTaget() {
        Rigidbody targetRb = target.GetComponent<Rigidbody>();
        if (targetRb == null)
            return target.position;

        float timeTointercept = Vector3.Distance(transform.position, target.position) / shoot.projectile.velocity;
        Vector3 predictedPos = target.position + (targetRb.velocity.normalized * targetRb.velocity.magnitude * timeTointercept);
        return predictedPos;
    }
}
