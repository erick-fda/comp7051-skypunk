﻿/*===========================================================================================
    Refs                                                                                *//**
	
	Static class for globally-accessible structs, enums, and other minor types.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
*//*=======================================================================================*/

/*===========================================================================================
	Dependencies
===========================================================================================*/
using UnityEngine;

/*===========================================================================================
	Refs
===========================================================================================*/
/**
	Namespace for globally-accessible structs, enums, and other minor types.
*/
public static class Refs
{
    /*===========================================================================================
		Filenames
	===========================================================================================*/
    /**
        Struct containing string constants for filenames.
    */
    public struct Filenames
    {
        public const string SaveDataFile = "SkyPunkSaveData.dat";
    }

    /*===========================================================================================
		Filepaths
	===========================================================================================*/
    /**
        Struct containing string constants for filepaths.
    */
    public struct Filepaths
    {
        public const string SaveDataFilepath = Refs.Directories.SaveDataDirectory + "/" + Refs.Filenames.SaveDataFile;
    }

    /*===========================================================================================
		Paths
	===========================================================================================*/
    /**
        Struct containing string constants for directories.
    */
    public struct Directories
    {
        public const string SaveDataDirectory = "/Saves";
    }

    /*===========================================================================================
		PlayerPrefs
	===========================================================================================*/
    /**
        Struct containing string constants for PlayerPrefs variable names.
    */
    public struct PlayerPrefs
    {
        public const string SubsequentPlayers = "SubsequentPlayers";
    }

    /*===========================================================================================
		Scenes
	===========================================================================================*/
    /**
        Struct containing string constants for scene names.
    */
    public struct Scenes
    {
        public const string MainMenu = "mainMenuScene";
        public const string Battle = "mainScene";
    }

    /*===========================================================================================
		Scoring
	===========================================================================================*/
    /**
        Struct containing constants for scoring constants.
    */
    public struct Scoring
    {
        public const int MinimumTeamScore = -10000;
        public const int MaximumTeamScore = 10000;
        public const int StartingTeamScore = 0;
    }

    /*===========================================================================================
		Tags
	===========================================================================================*/
    /**
        Struct containing string constants for GameObject tags.
    */
    public struct Tags
    {
        public const string Player = "Player";
    }
}
