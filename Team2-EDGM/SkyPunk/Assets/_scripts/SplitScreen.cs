﻿using UnityEngine;
using System.Collections;

public class SplitScreen : MonoBehaviour {

	public Camera camera;
	public GameObject scene;
	public int playerNumber;
	public int numberOfPlayers;

	// Use this for initialization
	void Start () {

        /* Set the scene variable. This is necessary because this variable will be null after a player respawns. */
        scene = GameObject.Find("Battle Scene");        

		playerNumber = transform.parent.gameObject.GetComponent<PlayerController> ().playerNumber;
		numberOfPlayers = scene.GetComponent<BattleScene>().Players.Count;

		switch (numberOfPlayers)
		{
		case 1:
			switch (playerNumber)
			{
			case 1:
				camera.rect = new Rect (0,0,1,1);
				break;
			}
			break;
		case 2:
			switch (playerNumber)
			{
			case 1:
				camera.rect = new Rect (0,0.5f,1,0.5f);
				break;
			case 2:
				camera.rect = new Rect (0,0,1,0.5f);
				break;
			}
			break;
		case 3:
			switch (playerNumber)
			{
			case 1:
				camera.rect = new Rect (0,0.5f,1,0.5f);
				break;
			case 2:
				camera.rect = new Rect (0,0,0.5f,0.5f);
				break;
			case 3:
				camera.rect = new Rect (0.5f,0,0.5f,0.5f);
				break;
			}
			break;
		case 4:
			switch (playerNumber)
			{
			case 1:
				camera.rect = new Rect (0,0.5f,0.5f,0.5f);
				break;
			case 2:
				camera.rect = new Rect (0.5f,0.5f,0.5f,0.5f);
				break;
			case 3:
				camera.rect = new Rect (0,0,0.5f,0.5f);
				break;
			case 4:
				camera.rect = new Rect (0.5f,0,0.5f,0.5f);
				break;
			}
			break;
		}
	}
}
