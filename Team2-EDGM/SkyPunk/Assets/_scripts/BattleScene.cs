﻿/*===========================================================================================
    BattleScene                                                                            *//**
	
	Manages a battle scene.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
*//*=======================================================================================*/

/*===========================================================================================
	Dependencies
===========================================================================================*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*===========================================================================================
	BattleScene
===========================================================================================*/
/**
	Manages a battle scene.
*/
public class BattleScene : MonoBehaviour
{
    /*=======================================================================================
		Fields
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		Game Object References
	---------------------------------------------------------------------------------------*/
    public GameObject Player1Prefab;            /**< Prefab to instantiate player 1 from. */
    public GameObject SubsequentPlayerPrefab;   /**< Prefab to instantiate subsequent players from. */
    public GameObject Team1SpawnPoint;          /**< Team 1's spawn point. */
    public GameObject Team2SpawnPoint;          /**< Team 2's spawn point. */
    public List<GameObject> Players;            /**< Players in numerical order. */
    public Text RoundTimer;                     /**< The timer that displays the amount of time left in the round. */
    public GameObject EndOfRoundDisplay;        /**< The end-of-round-display. */
    public Text EORDHeading;                    /**< End-of-round display heading text. */
    public Text EORDScores;                     /**< End-of-round display score text. */
    public Text EORDHighScore;                  /**< End-of-round-display high score notice. */
    public GameObject PauseScreen;              /**< Pause screen. */
    public Text PauseScreenScores;              /**< Pause screen score text. */

    /*---------------------------------------------------------------------------------------
		Public
	---------------------------------------------------------------------------------------*/


    /*---------------------------------------------------------------------------------------
		Private
	---------------------------------------------------------------------------------------*/
    private bool paused = false;            /**< Whether the scene is paused. */
    private float timeLeftInRound = 120;    /**< The time left in the round in seconds. */
    private List<int> teamScores = new List<int>
        { Refs.Scoring.StartingTeamScore, Refs.Scoring.StartingTeamScore }; /**< The current team scores. */
    
    /*---------------------------------------------------------------------------------------
		Protected
	---------------------------------------------------------------------------------------*/


    /*=======================================================================================
		Properties
	=======================================================================================*/
    /**
        Public access property for Scene.paused.

        @see Scene.paused
    */
    public bool Paused
    {
        get { return paused; }

        set
        {
            /* Set the paused value. */
            paused = value;

            /* Stop time when pausing and play time when unpausing. */
            Time.timeScale = (paused) ? 0 : 1;

            /* Disable PlayerControllers when pausing and enable them when unpausing. */
            SetPlayerControllersEnabled(!paused);
        }
    }

    /**
        Public access property for Game.teamScores.
        Private setting only.

        @see Game.teamScores
    */
    public List<int> TeamScores
    {
        get { return teamScores; }

        private set { teamScores = value; }
    }

    /**
        Public access property for Scene.timeLeftInRound.

        @see Scene.timeLeftInRound
    */
    public float TimeLeftInRound
    {
        get { return timeLeftInRound; }

        set { timeLeftInRound = value; }
    }

    /*=======================================================================================
		Methods
	=======================================================================================*/
    /*---------------------------------------------------------------------------------------
		MonoBehaviour Methods
	---------------------------------------------------------------------------------------*/
    /**
        Called once when the script is loaded.
        Used for setting up references to other scripts and enforcing singleton behaviours 
        on MonoBehaviours.

        Registers the appropriate number of players in the round.
    */
    void Awake()
    {
        RegisterPlayers();
    }

    /**
		Called once before the first frame update.
		Used for initializing referenced object variables and other values.
        
        Initializes the end-of-round display as inactive.
        Initializes the pause screen as inactive.
	*/
    void Start()
    {
        /* Set EORD and pause screen inactive. */
        EndOfRoundDisplay.SetActive(false);
        PauseScreen.SetActive(false);
    }

    /**
		Called once per frame.
		Used for general updating of game values and objects.

        If the round is not over yet, updates the round timer.
	*/
    void Update()
    {
        /* If the round is not over yet, update the round timer. */
        if (TimeLeftInRound > 0)
        {
            UpdateTimer();
        }

        /* If ESC key pressed, pause the game. */
        if (IRefs.GetKeyDown(IRefs.Command.Pause))
        {
            TogglePauseScreen();
        }
    }

    /*---------------------------------------------------------------------------------------
		Utility Methods
	---------------------------------------------------------------------------------------*/
    /**
        Destroys a player after the specified delay, respawning them if specified.

        @returns An IEnumerator of the specified delay before destroying and respawning 
                    the player.

        @param player - The player to destroy.
        @param deathDelay - The delay in seconds before destroying the player.
        @param respawn - Whether to respawn the player after destroying them. Defaults to 
                            true.
    */
    private IEnumerator DestroyPlayer(GameObject player, float deathDelay, bool respawn = true)
    {
        /* If the player is missing a Destructible script, throw an ArgumentException. */
        if (player.GetComponent<Destructible>() == null)
        {
            throw new System.ArgumentException("Argument player is missing a Destructible " +
                "script component.", "player");
        }

        /* Display the player's death message. */
        player.transform.Find("Canvas/Respawn Text").gameObject.SetActive(true);

        /* Wait for the specified delay. */
        yield return new WaitForSeconds(deathDelay);

        /* Destroy the player. */
        int playerNumber = player.GetComponent<PlayerController>().playerNumber;
        Destroy(player);

        /* Respawn the player at their team's spawn point if specified. */
        if (respawn)
        {
            /* If the player is player 1, respawn them with the player 1 prefab. */
            if (playerNumber == 1)
            {
                /* Respawn player 1 and update their reference in the player list. */
                Players[playerNumber - 1] = (GameObject)
                    Instantiate(Player1Prefab, Team2SpawnPoint.transform.position, Team2SpawnPoint.transform.rotation);
            }
            /* If the player is NOT player 1, respawn them with the subsequent player prefab. */
            else
            {
                /* Respawn player 1 and update their reference in the player list. */
                Players[playerNumber - 1] = (GameObject)
                    Instantiate(SubsequentPlayerPrefab, Team2SpawnPoint.transform.position, Team2SpawnPoint.transform.rotation);
            }
            
            /* Set the player number for the respawned player. */
            Players[playerNumber - 1].GetComponent<PlayerController>().playerNumber = playerNumber;
        }
        /* If the player will not be respawned, set their reference in the player list to null. */
        else
        {
            Players[playerNumber - 1] = null;
        }
    }
        
    /**
        Ends the round.

        Pauses the scene, updates the leaderboard, and displays the end-of-round-display.
    */
    public void EndRound()
    {
        /* Pause the scene. */
        Paused = true;

        /* Update leaderboard. */
        bool isNewHighScore = Game.Instance.Data.TryHighScore(TeamScores[1]);

        /* Save leaderboard to save file. */
        Game.Instance.SaveData();

        /* Show the end-of-round-display. */
        ShowEORD(isNewHighScore);
    }

    /**
        Handles a player death event.

        Disables the player controller, adjusts the team scores, destroys the player after 
        a delay, and then respawns them if specififed.

        @param player - The player that has died.
        @param scoreDelta - The change in the player team's score.
        @param deathDelay - The delay in seconds before destroying the player.
        @param respawn - Whether to respawn the player after destroying them. Defaults to 
                            true.
    */
    public void PlayerDeath(GameObject player, int scoreDelta, float deathDelay, bool respawn = true)
    {
        /* If the player is missing a PlayerController, throw an ArgumentException. */
        if (player.GetComponent<PlayerController>() == null)
        {
            throw new System.ArgumentException("Argument player is missing a " +
                "PlayerController script component.", "player");
        }

        /* Disable the player's PlayerController. */
        player.GetComponent<PlayerController>().enabled = false;

        /* Adjust the team scores. */
        ScoreBothTeams(2, scoreDelta);

        /* Start the coroutine to destroy (and possibly respawn) the player after a delay. */
        StartCoroutine(DestroyPlayer(player, deathDelay, respawn));
    }

    /**
        Registers the appropriate number ofplayers in the round.
    */
    private void RegisterPlayers()
    {
        /* Get the number of subsequent players to spawn. */
        int subsequentPlayers = PlayerPrefs.GetInt(Refs.PlayerPrefs.SubsequentPlayers);

        /* Set the position offset for subsequent players. */
        Vector3 offset = new Vector3(30, 0, 0);

        /* For each subsequent player, instantiate the player, set their player number, and 
            add them to the players list.*/
        for (int i = 1; i <= subsequentPlayers; i++)
        {
            /* Determine the new player's position. */
            Vector3 location = Team2SpawnPoint.transform.position + (offset * i); 

            /* Instantiate the player at the appropriate location. */
            GameObject newPlayer = (GameObject) Instantiate(SubsequentPlayerPrefab, location, Team2SpawnPoint.transform.rotation);

            /* Set the new player's player number. */
            newPlayer.GetComponent<PlayerController>().playerNumber = i + 1;

            /* Add the new player to the players list. */
            Players.Add(newPlayer);
        }
    }

    /**
        Scores both the player and enemy teams, applying scoreDelta to the team passed in 
        as a parameter and -scoreDelta to the other.

        @param deltaTeam - The team to apply scoreDelta to.
        @param scoreDelta - The change in score to apply to deltaTeam. -scoreDelta will 
                                be applied to the opposing team.
    */
    public void ScoreBothTeams(int deltaTeam, int scoreDelta)
    {
        /* The opposing team. */
        int oppTeam;

        /*  Determine the opposing team. */
        switch (deltaTeam)
        {
            case 1:
                oppTeam = 2;
                break;
            case 2:
                oppTeam = 1;
                break;
            default:
                throw new System.ArgumentOutOfRangeException("Argument team must be " +
                    "an int between 1 and 2.", "team");
        }

        ScoreTeam(deltaTeam, scoreDelta);
        ScoreTeam(oppTeam, -scoreDelta);
    }

    /**
        Adjusts the score of the specified team by scoreDelta.
        If specified, applies -scoreDelta to the opposing team's score.

        @param team - The team to apply scoreDelta to.
        @param scoreDelta - The change in score to apply.
    */
    public void ScoreTeam(int team, int scoreDelta)
    {
        /* If the team number is invalid, throw an ArgumentOutOfRangeException. */
        if ((team < 1) || (team > 2))
        {
            throw new System.ArgumentOutOfRangeException("Argument team must be " +
                "an int between 1 and 2.", "team");
        }

        /* Correct the team number to account for the zero-indexing of TeamScores. */
        team--;

        /* If the team's new score will be less than MIN_TEAM_SCORE, set the score to 
            MIN_TEAM_SCORE. Otherwise, adjust team's score. */
        if ((TeamScores[team] + scoreDelta) < Refs.Scoring.MinimumTeamScore)
        {
            TeamScores[team] = Refs.Scoring.MinimumTeamScore;
        }
        else
        {
            TeamScores[team] += scoreDelta;
        }
    }
        
    /**
        Enables or disables all PlayerControllers.

        @param enable - True to enable all PlayerControllers. False to disable them.
    */
    private void SetPlayerControllersEnabled(bool enable)
    {
        foreach (GameObject player in Players)
        {
            player.GetComponent<PlayerController>().enabled = enable;
        }
    }

    /**
        Shows the end-of-round-display.
        Shows the high score notice if specified.

        @param highScoreAchieved - Whether a high score was achieved by the player team.
    */
    private void ShowEORD(bool highScoreAchieved)
    {
        /* Temporary variables. */
        int playerTeamScore = TeamScores[1];
        int enemyTeamScore = TeamScores[0];
        string result;

        /* Determine the result. */
        if (playerTeamScore > enemyTeamScore)
        {
            result = "Player team wins!";
        }
        else if (playerTeamScore == enemyTeamScore)
        {
            result = "The two teams have tied!";
        }
        else
        {
            result = "Enemy team wins!";
        }

        /* Display EORD heading. */
        EORDHeading.text = "Round Over\n\n" + result;

        /* Display EORD scores. */
        EORDScores.text = "Player Team Score: " + TeamScores[1] + "\nEnemy Team Score: " + TeamScores[0];
        
        /* If specified, show the high score notice. */
        if (highScoreAchieved)
        {
            EORDHighScore.gameObject.SetActive(true);
        }

        /* Show the display. */
        EndOfRoundDisplay.SetActive(true);
    }

    /**
        Toggles the pause screen.
    */
    private void TogglePauseScreen()
    {
        /* Toggle the pause screen */
        PauseScreen.SetActive(!PauseScreen.activeSelf);
        Paused = PauseScreen.activeSelf;

        /* If the pause screen is now active */
        if (PauseScreen.activeSelf)
        {
            /* Display current scores. */
            PauseScreenScores.text = "Player Team Score: " + TeamScores[1] + "\nEnemy Team Score: " + TeamScores[0];
        }
    }

    /**
        Updates the round timer.
    */
    private void UpdateTimer()
    {
        /* Update the time left in the round. */
        TimeLeftInRound = TimeLeftInRound - (1 * Time.deltaTime);

        /* If the time remaining goes to zero or below, end the round. */
        if (TimeLeftInRound <= 0)
        {
            /* Disable the timer display. */
            RoundTimer.gameObject.SetActive(false);
            EndRound();
        }

        /* Update the timer display. */
        string timeLeft = "" +
            /* Minutes */
            Math.Floor(TimeLeftInRound / 60) + ":" +
            /* Seconds */
            Math.Floor(TimeLeftInRound % 60).ToString().PadLeft(2, '0');
        RoundTimer.text = timeLeft;
    }

    /*---------------------------------------------------------------------------------------
		Game Methods
	---------------------------------------------------------------------------------------*/
    /**
        Loads the specified scene.

        @param scene - The name of the scene to load.
    */
    public void LoadScene(string scene)
    {
        Game.Instance.LoadScene(scene);
    }
}
