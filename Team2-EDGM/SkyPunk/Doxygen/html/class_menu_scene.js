var class_menu_scene =
[
    [ "MenuViewNames", "struct_menu_scene_1_1_menu_view_names.html", "struct_menu_scene_1_1_menu_view_names" ],
    [ "Awake", "class_menu_scene.html#ad47da9e13ab08a36189bc0cce86f9936", null ],
    [ "FixedUpdate", "class_menu_scene.html#a096a8b8bb27befb882e18a251cbdb9cb", null ],
    [ "LateUpdate", "class_menu_scene.html#a0accd7df921c5afa8b973f8b770dce73", null ],
    [ "LoadScene", "class_menu_scene.html#aa4057a570701fce67671c08fdbccafd5", null ],
    [ "Quit", "class_menu_scene.html#a511eb64e9decba44329067c45755c049", null ],
    [ "SetLeaderboardDisplay", "class_menu_scene.html#aa6d7c3742be8c8b8eec20749078fd56c", null ],
    [ "SetMenuView", "class_menu_scene.html#a0503780e4d3fa0152ce39cf1996baf75", null ],
    [ "SetSubsequentPlayers", "class_menu_scene.html#ae49d47bf359bcae41fffa07c5f79c3e8", null ],
    [ "Start", "class_menu_scene.html#a496882efa14b0eae300dfe700d633c44", null ],
    [ "Update", "class_menu_scene.html#a037c28d6d4d84c19d0ef7a413817aa01", null ],
    [ "InstructionsView", "class_menu_scene.html#ab9fd471dce2279fdb9650dfa3ec7e1ac", null ],
    [ "LeaderboardDisplay", "class_menu_scene.html#af0cec197e185e2bd4d74c763129fb11e", null ],
    [ "LeaderboardView", "class_menu_scene.html#abec24aa50a86a09e95e2612f0f3ec069", null ],
    [ "MenuView", "class_menu_scene.html#a34f91cef01b350c96158202a6fa6c9b6", null ]
];