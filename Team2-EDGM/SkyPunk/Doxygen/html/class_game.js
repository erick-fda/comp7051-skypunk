var class_game =
[
    [ "Awake", "class_game.html#a18cc3d2c393f9ca2cabbc9ed4d727617", null ],
    [ "EnforceSingleton", "class_game.html#adfe447b05367e5af4cfd28cf63704df8", null ],
    [ "InitSaveData", "class_game.html#af9a76795f8e79bc171e90ce317fb14fc", null ],
    [ "LoadScene", "class_game.html#a3a654158b1f752ae7f4f9479883982c5", null ],
    [ "Quit", "class_game.html#ad8df470c569d4ed80aa973fd08e6efac", null ],
    [ "SaveData", "class_game.html#a7b7f543905bb63f60e7c9e4540464d4a", null ],
    [ "SetSubsequentPlayers", "class_game.html#aad250f5d491adc71a56c7ac22799d64a", null ],
    [ "data", "class_game.html#ab08e9bb02419b9eafdedacb3d8a46541", null ],
    [ "instance", "class_game.html#a60205c5d70bb7159f373eb205ebdcbba", null ],
    [ "Data", "class_game.html#a7c0aeabd0409b3ddaf56085fec4f7a54", null ],
    [ "Instance", "class_game.html#a768ab7c51eafee90672f1c644e66b2c0", null ]
];