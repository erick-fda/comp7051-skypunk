var searchData=
[
  ['initsavedata',['InitSaveData',['../class_game.html#af9a76795f8e79bc171e90ce317fb14fc',1,'Game']]],
  ['inputsource',['InputSource',['../class_i_refs.html#a9d1f3e8e9c6120972f1934acd6395628',1,'IRefs']]],
  ['instance',['instance',['../class_game.html#a60205c5d70bb7159f373eb205ebdcbba',1,'Game.instance()'],['../class_game.html#a768ab7c51eafee90672f1c644e66b2c0',1,'Game.Instance()']]],
  ['instructionsview',['InstructionsView',['../struct_menu_scene_1_1_menu_view_names.html#a50b527feb2b3c435da65fb81c2f52f2b',1,'MenuScene.MenuViewNames.InstructionsView()'],['../class_menu_scene.html#ab9fd471dce2279fdb9650dfa3ec7e1ac',1,'MenuScene.InstructionsView()']]],
  ['irefs',['IRefs',['../class_i_refs.html',1,'']]],
  ['irefs_2ecs',['IRefs.cs',['../_i_refs_8cs.html',1,'']]],
  ['isalive',['isAlive',['../class_destructible.html#aab33cef9d1da7ddbd1869a89261ca0cc',1,'Destructible']]],
  ['isdamaged',['isDamaged',['../class_destructible.html#a6f92c370c1cc814904ef607f3ad494c4',1,'Destructible']]],
  ['isfriendly',['isFriendly',['../class_label_controller.html#afa68cfebbbdae78b6569791d3572caf1',1,'LabelController']]],
  ['isparticlesystem',['isParticleSystem',['../class_shoot.html#a8fea28effa200bb1569bcf5332434a1f',1,'Shoot']]]
];
