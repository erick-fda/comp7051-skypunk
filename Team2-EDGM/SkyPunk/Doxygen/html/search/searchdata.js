var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvwxy",
  1: "abcdfgilmprstu",
  2: "abcdgilmprst",
  3: "acdefgilmopqrstu",
  4: "abcdefghijklmnoprstvwy",
  5: "cij",
  6: "abdefjklnprstxy",
  7: "dilpt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties"
};

