var class_ship_a_i =
[
    [ "attackTarget", "class_ship_a_i.html#ad4474725075481ce2fd9a06d75cfc8f6", null ],
    [ "avoidCollision", "class_ship_a_i.html#a71e2040adcfd8bacf1d311c6938cbafe", null ],
    [ "FixedUpdate", "class_ship_a_i.html#af46ed27bf7411d7818b0cc9001f5a935", null ],
    [ "mainMovementController", "class_ship_a_i.html#ac7de6cfc08400fa0a1566bba57d5eff5", null ],
    [ "rollUpright", "class_ship_a_i.html#ac1660f49f80d61e8b026ce81a3c11bbd", null ],
    [ "Start", "class_ship_a_i.html#a95f1fe315fa518d5b7bc5039b008a149", null ],
    [ "avoidanceDistance", "class_ship_a_i.html#ac5ae3e69ebb9c483263d495ca3a2f1d1", null ],
    [ "closestContactDir", "class_ship_a_i.html#ae92c6d262edda2979610c4d72cc1e2ae", null ],
    [ "maneuverSpeedMultiplier", "class_ship_a_i.html#ab7ed23bdb82571c1d8299c73aba1e852", null ],
    [ "minDistance", "class_ship_a_i.html#a4970ab49ae54ec712c6427d558738b45", null ],
    [ "overSteer", "class_ship_a_i.html#ac051dee1a4634cbb0c9ef53760be6242", null ],
    [ "ownCollider", "class_ship_a_i.html#a87dae540310150f852b96bc96ced6713", null ],
    [ "rb", "class_ship_a_i.html#af82930078502ef341239a83eb52ab4b5", null ],
    [ "rightingSpeed", "class_ship_a_i.html#a8939ed8a5afe63355cb2f206dc82b789", null ],
    [ "sensorMargin", "class_ship_a_i.html#a416f69c5ca42035000b58dade5bc4b2c", null ],
    [ "thrust", "class_ship_a_i.html#aaff96f71cf2c8cb8aeadcf49578c3582", null ],
    [ "timeSinceAvoidance", "class_ship_a_i.html#ae2002b3e1e4c89607bec9b237d53471a", null ],
    [ "topSpeed", "class_ship_a_i.html#a1248601edde96f8fd0fb5e14d26144af", null ],
    [ "turnSpeed", "class_ship_a_i.html#a86f3d81b7bd0b5a0ffb27ae8719a6488", null ]
];