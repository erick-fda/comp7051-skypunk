var dir_73676bdeaeb2df680f30d0ff04b27f4f =
[
    [ "IRefs.cs", "_i_refs_8cs.html", [
      [ "IRefs", "class_i_refs.html", "class_i_refs" ],
      [ "UnsupportedPlatformException", "class_i_refs_1_1_unsupported_platform_exception.html", "class_i_refs_1_1_unsupported_platform_exception" ]
    ] ],
    [ "Refs.cs", "_refs_8cs.html", [
      [ "Refs", "class_refs.html", [
        [ "Directories", "struct_refs_1_1_directories.html", "struct_refs_1_1_directories" ],
        [ "Filenames", "struct_refs_1_1_filenames.html", "struct_refs_1_1_filenames" ],
        [ "Filepaths", "struct_refs_1_1_filepaths.html", "struct_refs_1_1_filepaths" ],
        [ "PlayerPrefs", "struct_refs_1_1_player_prefs.html", "struct_refs_1_1_player_prefs" ],
        [ "Scenes", "struct_refs_1_1_scenes.html", "struct_refs_1_1_scenes" ],
        [ "Scoring", "struct_refs_1_1_scoring.html", "struct_refs_1_1_scoring" ],
        [ "Tags", "struct_refs_1_1_tags.html", "struct_refs_1_1_tags" ]
      ] ],
      [ "Filenames", "struct_refs_1_1_filenames.html", "struct_refs_1_1_filenames" ],
      [ "Filepaths", "struct_refs_1_1_filepaths.html", "struct_refs_1_1_filepaths" ],
      [ "Directories", "struct_refs_1_1_directories.html", "struct_refs_1_1_directories" ],
      [ "PlayerPrefs", "struct_refs_1_1_player_prefs.html", "struct_refs_1_1_player_prefs" ],
      [ "Scenes", "struct_refs_1_1_scenes.html", "struct_refs_1_1_scenes" ],
      [ "Scoring", "struct_refs_1_1_scoring.html", "struct_refs_1_1_scoring" ],
      [ "Tags", "struct_refs_1_1_tags.html", "struct_refs_1_1_tags" ]
    ] ]
];