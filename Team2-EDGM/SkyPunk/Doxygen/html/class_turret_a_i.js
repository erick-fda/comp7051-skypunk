var class_turret_a_i =
[
    [ "aim", "class_turret_a_i.html#a88747799ed0b60be278ba1fdb846f34b", null ],
    [ "leadTaget", "class_turret_a_i.html#a0ede68b69ad96a59e0e4f6b1913027d4", null ],
    [ "Start", "class_turret_a_i.html#abc482aaf4087a127fda56cc4e7192175", null ],
    [ "Update", "class_turret_a_i.html#a72b1e4012d0de3b6eb4b1999c3fe1505", null ],
    [ "elevationSpeed", "class_turret_a_i.html#a4819f694963751afe5d128a9a977cb3b", null ],
    [ "gunBarrel", "class_turret_a_i.html#a84ae3f657cab02cb33e4d3548b00e817", null ],
    [ "gunDepression", "class_turret_a_i.html#a98755290ee7b4db22a6186b90a83c6aa", null ],
    [ "turnSpeed", "class_turret_a_i.html#a158ddb8eecdc5379ad0ef4303f9e8746", null ],
    [ "turretBase", "class_turret_a_i.html#a2ae02bcabde428470c69d46962c64669", null ]
];