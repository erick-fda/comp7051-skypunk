var annotated_dup =
[
    [ "AI", "class_a_i.html", "class_a_i" ],
    [ "AnimationPlayer", "class_animation_player.html", "class_animation_player" ],
    [ "BattleScene", "class_battle_scene.html", "class_battle_scene" ],
    [ "Console", "class_console.html", "class_console" ],
    [ "CreatureAnims", "class_creature_anims.html", "class_creature_anims" ],
    [ "Destructible", "class_destructible.html", "class_destructible" ],
    [ "DestructibleDeathTest", "class_destructible_death_test.html", "class_destructible_death_test" ],
    [ "DontFlyAway", "class_dont_fly_away.html", "class_dont_fly_away" ],
    [ "Game", "class_game.html", "class_game" ],
    [ "IRefs", "class_i_refs.html", "class_i_refs" ],
    [ "LabelController", "class_label_controller.html", "class_label_controller" ],
    [ "MenuScene", "class_menu_scene.html", "class_menu_scene" ],
    [ "PlayerController", "class_player_controller.html", "class_player_controller" ],
    [ "Projectile", "class_projectile.html", "class_projectile" ],
    [ "Refs", "class_refs.html", [
      [ "Directories", "struct_refs_1_1_directories.html", "struct_refs_1_1_directories" ],
      [ "Filenames", "struct_refs_1_1_filenames.html", "struct_refs_1_1_filenames" ],
      [ "Filepaths", "struct_refs_1_1_filepaths.html", "struct_refs_1_1_filepaths" ],
      [ "PlayerPrefs", "struct_refs_1_1_player_prefs.html", "struct_refs_1_1_player_prefs" ],
      [ "Scenes", "struct_refs_1_1_scenes.html", "struct_refs_1_1_scenes" ],
      [ "Scoring", "struct_refs_1_1_scoring.html", "struct_refs_1_1_scoring" ],
      [ "Tags", "struct_refs_1_1_tags.html", "struct_refs_1_1_tags" ]
    ] ],
    [ "SaveData", "class_save_data.html", "class_save_data" ],
    [ "SceneOverTest", "class_scene_over_test.html", "class_scene_over_test" ],
    [ "ShipAI", "class_ship_a_i.html", "class_ship_a_i" ],
    [ "Shoot", "class_shoot.html", "class_shoot" ],
    [ "SplitScreen", "class_split_screen.html", "class_split_screen" ],
    [ "TurretAI", "class_turret_a_i.html", "class_turret_a_i" ]
];