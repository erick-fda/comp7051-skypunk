var class_label_controller =
[
    [ "Start", "class_label_controller.html#ac46d09dc511ab409c779adb296aa6ca7", null ],
    [ "Update", "class_label_controller.html#a58d88167b74c6dd63441adbc7d82dc87", null ],
    [ "isFriendly", "class_label_controller.html#afa68cfebbbdae78b6569791d3572caf1", null ],
    [ "labelColor", "class_label_controller.html#ad1c489905e82ec822876f63c825c2733", null ],
    [ "labelText", "class_label_controller.html#a2937bc0b08e5c28db4b1720303a2d628", null ],
    [ "mainCamera", "class_label_controller.html#a2b6e9523b12d706961c26272d31fb13e", null ],
    [ "nameString", "class_label_controller.html#a5b526cdd456380157bf568a20f9ad0e0", null ],
    [ "textGameobject", "class_label_controller.html#a5b4e1ef02ba163266dab13a33354f6f1", null ]
];