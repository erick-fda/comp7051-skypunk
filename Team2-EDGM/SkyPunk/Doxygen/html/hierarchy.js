var hierarchy =
[
    [ "Refs.Directories", "struct_refs_1_1_directories.html", null ],
    [ "Exception", null, [
      [ "IRefs.UnsupportedPlatformException", "class_i_refs_1_1_unsupported_platform_exception.html", null ]
    ] ],
    [ "Refs.Filenames", "struct_refs_1_1_filenames.html", null ],
    [ "Refs.Filepaths", "struct_refs_1_1_filepaths.html", null ],
    [ "IRefs", "class_i_refs.html", null ],
    [ "MenuScene.MenuViewNames", "struct_menu_scene_1_1_menu_view_names.html", null ],
    [ "MonoBehaviour", null, [
      [ "AI", "class_a_i.html", [
        [ "ShipAI", "class_ship_a_i.html", null ],
        [ "TurretAI", "class_turret_a_i.html", null ]
      ] ],
      [ "AnimationPlayer", "class_animation_player.html", null ],
      [ "BattleScene", "class_battle_scene.html", null ],
      [ "Console", "class_console.html", null ],
      [ "CreatureAnims", "class_creature_anims.html", null ],
      [ "Destructible", "class_destructible.html", null ],
      [ "DestructibleDeathTest", "class_destructible_death_test.html", null ],
      [ "DontFlyAway", "class_dont_fly_away.html", null ],
      [ "Game", "class_game.html", null ],
      [ "LabelController", "class_label_controller.html", null ],
      [ "MenuScene", "class_menu_scene.html", null ],
      [ "PlayerController", "class_player_controller.html", null ],
      [ "Projectile", "class_projectile.html", null ],
      [ "SceneOverTest", "class_scene_over_test.html", null ],
      [ "Shoot", "class_shoot.html", null ],
      [ "SplitScreen", "class_split_screen.html", null ]
    ] ],
    [ "Object", null, [
      [ "SaveData", "class_save_data.html", null ]
    ] ],
    [ "Refs.PlayerPrefs", "struct_refs_1_1_player_prefs.html", null ],
    [ "Refs", "class_refs.html", null ],
    [ "Refs.Scenes", "struct_refs_1_1_scenes.html", null ],
    [ "Refs.Scoring", "struct_refs_1_1_scoring.html", null ],
    [ "Refs.Tags", "struct_refs_1_1_tags.html", null ]
];