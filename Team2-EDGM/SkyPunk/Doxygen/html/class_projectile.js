var class_projectile =
[
    [ "OnCollisionEnter", "class_projectile.html#a9e6d0b06970a4032430f10096c9ca4a8", null ],
    [ "Start", "class_projectile.html#ac7e861ae0d04774ce1ce5832dcd2281f", null ],
    [ "Update", "class_projectile.html#a003e4365f5c68afdebfbefa39a3fd7bc", null ],
    [ "damage", "class_projectile.html#af3269892d37d6cb0f551f84f7197d882", null ],
    [ "lifeTime", "class_projectile.html#ab5322354a151820e33247f075eb758b3", null ],
    [ "owner", "class_projectile.html#ac081071adb2faaf06096a2f9f794cecd", null ],
    [ "velocity", "class_projectile.html#a675beaf0b80b107bc72ba6eccadc9a63", null ]
];